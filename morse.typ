#let morse(s, unit: 1, trailing: true) = {
	let ord(c) = {
		let b = bytes(c)
		if b.len() == 1 {
			b.at(0)
		}
	}

	let char-lower(c) = {
		let b = bytes(c)
		if b.len() == 1 {
			let n = ord(c)
			if ord("A") <= n and n <= ord("Z") {
				str(bytes((n + ord("a") - ord("A"),)))
			} else {
				c
			}
		} else {
			c
		}
	}

	let lookup = (
		"a": ".-",
		"b": "-...",
		"c": "-.-.",
		"d": "-..",
		"e": ".",
		"f": "..-.",
		"g": "--.",
		"h": "....",
		"i": "..",
		"j": ".---",
		"k": "-.-",
		"l": ".-..",
		"m": "--",
		"n": "-.",
		"o": "---",
		"p": ".--.",
		"q": "--.-",
		"r": ".-.",
		"s": "...",
		"t": "-",
		"u": "..-",
		"v": "...-",
		"w": ".--",
		"x": "-..-",
		"y": "-.--",
		"z": "--..",
		"0": "-----",
		"1": ".----",
		"2": "..---",
		"3": "...--",
		"4": "....-",
		"5": ".....",
		"6": "-....",
		"7": "--...",
		"8": "---..",
		"9": "----.",
		".": ".-.-.-",
		",": "--..--",
		"?": "..--..",
		"'": ".----.",
		"!": "-.-.--",
		"/": "-..-.",
		"(": "-.--.",
		")": "-.--.-",
		"&": ".-...",
		":": "---...",
		";": "-.-.-.",
		"=": "-...-",
		"+": ".-.-.",
		"-": "-....-",
		"_": "..--.-",
		"\u{0022}": ".-..-.",
		"$": "...-..-",
		"@": ".--.-.",
	)

	let res = ()
	for word in s.split() {
		for c in word.codepoints() {
			let code = lookup.at(char-lower(c))
			for m in code.codepoints() {
				let val = (".": unit, "-": 3 * unit).at(m)
				res.push(val)
				res.push(unit)
			}
			let _ = res.pop()
			res.push(3 * unit)
		}
		let _ = res.pop()
		res.push(7 * unit)
	}
	if not trailing {
		let _ = res.pop()
	}
	res
}

// make the line exactly one message long
#let message = morse("Hello, World!", unit: 1pt, trailing: false)
#line(length: message.sum(), stroke: (dash: message,))\ 
// or have it repeat forever (trailing space is needed for correct spacing between repetitions)
#let message = morse("Hello, World!", unit: 1pt, trailing: true)
#line(length: 100%, stroke: (dash: message,))\ 